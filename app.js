require('dotenv').config();

const sACN = require('stagehack-sacn').Receiver;
const redis = require('redis');

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
});

const channelOffsets = {textId: 0, red: 1, green: 2, blue: 3};
const channelCount = Object.keys(channelOffsets).length;

if (process.env.REDIS_PASSWORD !== undefined) {
  redisClient.auth(process.env.REDIS_PASSWORD);
}

let clientData = [];

for (let i = 0; i < process.env.COLOR_CHANNELS_COUNT; i++) {
  clientData.push({color: {red: 0, green: 0, blue: 0}, textId: 255});
}

let previousClientData = clientData.map(clientData => {
  return {...clientData};
});

sACN.Start();
const universe = new sACN.Universe([process.env.SACN_UNIVERSE]);

universe.on('packet', (packet) => {
  const data = packet.getSlots();
  clientData.map((channel, index) => {
    clientData[index] = {
      color: {
        red: data[(index * channelCount) + channelOffsets.red],
        green: data[(index * channelCount) + channelOffsets.green],
        blue: data[(index * channelCount) + channelOffsets.blue],
      },
      textId: data[(index * channelCount) + channelOffsets.textId],
    };
  });
});

console.log('Started sACN receiver listening to ' + process.env.SACN_UNIVERSE);


function sendColors() {
  let isChanged = false;
  clientData.forEach((channel, index) => {
    if (channel.color.red !== previousClientData[index].color.red || channel.color.green !== previousClientData[index].color.green || channel.color.blue !== previousClientData[index].color.blue || channel.textId !== previousClientData[index].textId) {
      isChanged = true;
    }
  });
  if (!isChanged) {
    // Nothing has changed
    setTimeout(sendColors, process.env.SENDING_TIMEOUT);
    return;
  }

  // Publish color changes to redis
  redisClient.publish('colorUpdate', JSON.stringify({clientData}));

  previousClientData = clientData.map(clientData => {
    return {...clientData};
  });
  setTimeout(sendColors, process.env.SENDING_TIMEOUT);
}

sendColors();
