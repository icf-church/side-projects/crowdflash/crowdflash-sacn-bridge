# Crowdflash

Crowdflash is an application that allows displaying colors on many smartphones and tablets at the same time, controlled via DMX interfaces.

Crowdflash consists of four main components that interact with one another in real time.

- Crowdflash Bridge
  - reads input via [ArtNet](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-artnet-bridge) or [sACN](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-sacn-bridge) and publishes updates to Redis
- Redis Pub/Sub Server
- [Crowdflash Websocket Server](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-pubsub-server)
  - subscribes to Redis and pushes updates to all connected websocket clients
- [Crowdflash Client](https://gitlab.com/icf-church/side-projects/crowdflash/crowdflash-client)
  - displays the received colors on the screen

# Crowdflash sACN Bridge

The Crowdflash sACN Bridge is a simple DMX receiver that publishes any changes to a Redis server. Changes are saved instantly, but published only every x milliseconds to avoid flooding the Redis with too many requests. The [sACN protocol](https://artisticlicenceintegration.com/technology-brief/technology-resource/sacn-and-art-net/) used is standardized under ANSI E1.31-2016 and often referred to as E1.31.

Even though it should work with any device capable of sending sACN, the bridge is currently only tested with the following devices. I would be very interested in hearing from you if you're running it with other devices.

- GrandMA2 console
- [Art-Net Controller](https://play.google.com/store/apps/details?id=com.litux.art_net_controller) (Android app)

## Requirements

This app requires Node.js to run. It is tested with node v16.x, but should run with lower versions just fine.
The sACN receiver needs to be reachable from the network that the sending sACN device is in, while also having access to the Redis server which is running on the internet. For us it works great to run it on a VM within our event network, but running it on a laptop with WiFi or a Raspberry PI with internet connection should do the trick as well.

## Installation

Download the source via git or as a zip.
Install requirements by running

```bash
npm ci
```

## Usage

Running the app can be as simple as executing

```bash
node app.js
```

For a more permanent solution however consider using a process manager like [pm2](https://pm2.keymetrics.io/).

## Business requests
If you would like to use Crowdflash at your event, but don't feel like setting up and managing the required infrastructure yourself, we would be more than happy to talk to you! Feel free to reach out to us via email at [web@icf.ch](mailto:web@icf.ch)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/) - see [LICENSE](LICENSE) file
